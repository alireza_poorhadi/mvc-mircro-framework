<?php

namespace App\Models;

use App\Models\contracts\MysqlBaseModel;

class User extends MysqlBaseModel
{
    protected $table = 'users';
}