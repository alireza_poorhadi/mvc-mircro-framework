<?php

namespace App\Controllers;

use App\Models\User;

class PostController
{
    public function single()
    {
        global $request;
        
        
        $slug = $request->routeParam('slug');
        echo "Show single post $slug";
    }

    public function comment() 
    {
        global $request;
        $cid = $request->routeParam('cid');
        echo "show single comment $cid";
    }
}
