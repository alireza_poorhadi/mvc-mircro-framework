<?php

function siteURL($route)
{
    return $_ENV['BASE_URL'] . $route;
}

function view($path) # errors.404
{
    $path = str_replace('.', '/', $path); # errors/404
    include_once BASEPATH . 'views/' . $path . '.php';
}

function niceDump($var)
{
    echo '<pre>';
    echo '<div style="margin: 20px auto; border: 1px solid maroon; border-left: 7px solid maroon; border-radius: 5px; padding: 10px; width: 95%;">';
    var_dump($var);
    echo '</div>';
    echo '</pre>';
}

function dd($var)
{
    niceDump($var);
    die();
}
