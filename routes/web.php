<?php

use App\Core\Routing\Route;

Route::get('/', function () {
    echo "welcome";
});

Route::get('/null', '');

Route::add(['get', 'post'], '/a', 'HomeController@index');

Route::get('/b', ['HomeController', 'index']);

Route::get('/post/{slug}', 'PostController@single');

Route::get('/post/{slug}/comment/{cid}', 'PostController@comment');
