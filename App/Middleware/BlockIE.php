<?php

namespace App\Middleware;

use App\Middleware\contracts\MiddlewareInterface;

class BlockIE implements MiddlewareInterface {
    public function handle()
    {
        echo 'IE blocked.';
    }
}